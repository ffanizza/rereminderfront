package com.iit.rereminder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Frank on 2016-11-25.
 */

public class GlobalDataUnitTests {

    GlobalData unitTests = new GlobalData();

    @Test
    public void convertFormatHourTestIsCorrect() throws Exception {
        assertEquals(12,unitTests.convertFormatHourTest(0));
        assertEquals(1,unitTests.convertFormatHourTest(1));
        assertEquals(12,unitTests.convertFormatHourTest(12));
        assertEquals(1,unitTests.convertFormatHourTest(13));
    }

    @Test
    public void minuteToStringIsCorrect() throws Exception {
        assertEquals("00",unitTests.minuteToString(0));
        assertEquals("05",unitTests.minuteToString(5));
        assertEquals("10",unitTests.minuteToString(10));
        assertEquals("55",unitTests.minuteToString(55));
    }

    @Test
    public void determineAmPmIsCorrect() throws Exception {
        assertEquals("AM",unitTests.determineAmPm(0));
        assertEquals("AM",unitTests.determineAmPm(11));
        assertEquals("PM",unitTests.determineAmPm(12));
        assertEquals("PM",unitTests.determineAmPm(23));
    }

    @Test
    public void writtenDateToStdIsCorrect() throws Exception {
        assertEquals("2016-01-01",unitTests.writtenDateToStd("January 1, 2016"));
        assertEquals("2015-12-31",unitTests.writtenDateToStd("December 31, 2015"));
    }

    @Test
    public void usTimeToTwentyfourIsCorrect() throws Exception {
        assertEquals("00:00",unitTests.usTimeToTwentyfour("12:00 AM"));
        assertEquals("01:05",unitTests.usTimeToTwentyfour("1:05 AM"));
        assertEquals("01:10",unitTests.usTimeToTwentyfour("1:10 AM"));
        assertEquals("11:10",unitTests.usTimeToTwentyfour("11:10 AM"));
        assertEquals("12:00",unitTests.usTimeToTwentyfour("12:00 PM"));
        assertEquals("13:00",unitTests.usTimeToTwentyfour("1:00 PM"));
        assertEquals("23:59",unitTests.usTimeToTwentyfour("11:59 PM"));
    }

    @Test
    public void convertMilliToYYYYMMddIsCorrect() throws Exception {
        assertEquals(true,unitTests.convertMilliToYYYYMMdd(1500000000).matches(
                "[0-9]{4}-([0][0-9]|[1][012])-([012][0-9]|[3][01])"));
    }

    @Test
    public void convertMilliToHHMMIsCorrect() throws Exception {
        assertEquals(true,unitTests.convertMilliToHHmm(1500000000).matches(
                "([01][0-9]|[2][0-3]):[0-5][0-9]"));
    }




}
