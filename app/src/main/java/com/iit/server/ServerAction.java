package com.iit.server;

import android.os.StrictMode;

import java.io.OutputStream;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.Proxy;
import java.net.InetSocketAddress;
import java.io.OutputStreamWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerAction implements ServerConnection {

    //the server domain;
    String host = "http://rereminder.herokuapp.com/";

    //user related
    //------------------------------------ createUser ------------------------------------
    public String createUserRequest(String username, String email, String password) {

        JSONObject jo = new JSONObject();
        String userUrl = host + "users";
        String result = null;

        try {
            jo.put("username", username.toLowerCase());
            jo.put("email", email);
            jo.put("password", password);
        } catch (final JSONException e) {
        }


        try {
            URL url = new URL(userUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

            osw.write(jo.toString());
            osw.flush();

            //get the response
            StringBuilder sb = new StringBuilder();
            int status = conn.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
                String tempStr = null;
                while ((tempStr = br.readLine()) != null) {
                    sb.append(tempStr);
                }
                br.close();
                result = sb.toString();
            } else {
                result = conn.getResponseMessage();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		System.out.println(result);
        return result;
    }

	//------------------------------------ login ------------------------------------
	public String loginRequest(String username, String password){return "true";}
    // TODO username must be lowercase to match the username the user register with
	//currently does nothing.

	//course related
	//------------------------------------ addCourse ------------------------------------
	public String addCourseRequest(String username,String courseName, String startTime, String endTime, ArrayList<String> days){
		JSONObject jo = new JSONObject();
		String courseUrl = host +  "users/" + username + "/courses";
		String result = null;

        try {
            jo.put("name", courseName);
            jo.put("startTime", startTime);
            jo.put("endTime", endTime);
            jo.put("days", new JSONArray(days));
        }catch (final JSONException e) {}

		try{
			URL url = new URL(courseUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			OutputStream os = conn.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

			osw.write(jo.toString());
			osw.flush();

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//------------------------------------ modifyCourse ------------------------------------
	public String modifyCourseRequest(String username, int courseId, String courseName, String startTime, String endTime, ArrayList<String> days){
		String result = null;
		JSONObject jo = new JSONObject();
		String courseUrl = host +  "users/" + username + "/courses/" + courseId;

		try {
            jo.put("name", courseName);
            jo.put("startTime", startTime);
            jo.put("endTime", endTime);
            jo.put("days", new JSONArray(days));
        }catch (final JSONException e) {}


		try{
			URL url = new URL(courseUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");

			OutputStream os = conn.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

			osw.write(jo.toString());
			osw.flush();

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//------------------------------------ dropCourse ------------------------------------
	public String dropCourseRequest(String username, int courseId){
		String courseUrl = host +  "users/" + username + "/courses/" + courseId;
		String result = null;

		try{
			URL url = new URL(courseUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setRequestMethod("DELETE");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//------------------------------------ getCourses ------------------------------------
	public String getCoursesRequest(String username){
		String courseUrl = host +  "users/" + username + "/courses";
		String result = null;

		try{
			URL url = new URL(courseUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			//conn.setDoOutput(true); // TODO REMOVE IT ! it's for post and put, not get
			conn.setRequestMethod("GET");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			System.out.println(status);
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return "{courses:" + result + "}";
	}

	//------------------------------------ getCourseDetails ------------------------------------
	public String getCourseDetailsRequest(String username, int courseId){
		String courseUrl = host +  "users/" + username + "/courses/" + courseId;
        System.out.println(courseUrl);
		String result = null;

		try{
			URL url = new URL(courseUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			//conn.setDoOutput(true); TODO REMOVE IT
			conn.setRequestMethod("GET");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//reminder related
	//------------------------------------ addReminder ------------------------------------
	public String addReminderRequest(String username, String content, String courseName, String dueDate, String dueTime){
		JSONObject jo = new JSONObject();
		String reminderUrl = host +  "users/" + username + "/reminders";
		String result = null;

		try {
            jo.put("content", content);
            jo.put("course", courseName);
            jo.put("date", dueDate);
			jo.put("time", dueTime);
        } catch (final JSONException e) {}

		try{
			URL url = new URL(reminderUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			OutputStream os = conn.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

			osw.write(jo.toString());
			osw.flush();

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//------------------------------------ modifyReminder ------------------------------------
	public String modifyReminderRequest(String username, int reminderId, String content, String courseName, String dueDate, String dueTime){
		JSONObject jo = new JSONObject();
		String reminderUrl = host +  "users/" + username + "/reminders/" + reminderId;
		String result = null;

        try{
            jo.put("content", content);
            jo.put("course", courseName);
            jo.put("dueDate", dueDate);
			jo.put("dueTime", dueTime);
        } catch (final JSONException e) {}


		try{
			URL url = new URL(reminderUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			OutputStream os = conn.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

			osw.write(jo.toString());
			osw.flush();

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//------------------------------------ completeReminder ------------------------------------
	public String completeReminderRequest(String username, int reminderId){
		String reminderUrl = host +  "users/" + username + "/reminders/" + reminderId;
		String result = null;

		try{
			URL url = new URL(reminderUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//------------------------------------ getActiveReminders ------------------------------------
	public String getActiveRemindersRequest(String username){
		String reminderUrl = host +  "users/" + username + "/reminders";
		String result = null;

		try{
			URL url = new URL(reminderUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			//conn.setDoOutput(true); TODO REMOVE IT
			conn.setRequestMethod("GET");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
				System.out.println("Doesn t work");
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return "{reminders:" + result + "}";
	}

	//------------------------------------ getReminderDetails ------------------------------------
	public String getReminderDetails(String username, int reminderId){
		String reminderUrl = host +  "users/" + username + "/reminders/" + reminderId;
		String result = null;

		try{
			URL url = new URL(reminderUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			//conn.setDoOutput(true); TODO REMOVE IT
			conn.setRequestMethod("GET");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}

	//notification related
	//------------------------------------ getNotifications ------------------------------------
	public String getNotificationsRequest(String username){
		String notiUrl = host +  "users/" + username + "/notifications/";
		String result = null;

		try{
			URL url = new URL(notiUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			//conn.setDoOutput(true); TODO REMOVE IT
			conn.setRequestMethod("GET");

			//get the response
			StringBuilder sb = new StringBuilder();
			int status = conn.getResponseCode();
			if(status == HttpURLConnection.HTTP_OK){
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String tempStr = null;
				while((tempStr = br.readLine()) != null){
					sb.append(tempStr);
				}
				br.close();
				result = sb.toString();
			}else{
				result = conn.getResponseMessage();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return result;
	}
}