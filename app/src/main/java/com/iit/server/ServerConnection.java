//Only for testing ReReminder
package com.iit.server;

import java.util.ArrayList;

public interface ServerConnection{
	//user related
	public String createUserRequest(String username, String email, String passwrod);
	public String loginRequest(String username, String password);
	//course related
	public String addCourseRequest(String username,String courseName, String startTime, String endTime, ArrayList<String> days);
	public String modifyCourseRequest(String username, int courseId, String courseName, String startTime, String endTime, ArrayList<String> days);
	public String dropCourseRequest(String username, int courseId);
	public String getCoursesRequest(String username);
	public String getCourseDetailsRequest(String username, int courseId);
	//reminder related
	public String addReminderRequest(String username, String content, String courseName, String dueDate, String dueTime);
	public String modifyReminderRequest(String username, int reminderId, String content, String courseName, String dueDate, String dueTime);
	public String completeReminderRequest(String username, int reminderId);
	public String getActiveRemindersRequest(String username);
	public String getReminderDetails(String username, int reminderId);
	//notification related
	public String getNotificationsRequest(String username);
}