package com.iit.rereminder;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

/**
 * Created by Frank on 2016-10-31.
 */

public class TimePickerDialogFragment extends DialogFragment {

    Handler timePickHandler;
    int hour;
    int min;

    public Handler getHandler() {
        return timePickHandler;
    }
    public void setHandler(Handler handler) {
        this.timePickHandler = timePickHandler;
    }


    public TimePickerDialogFragment () {

    }



//    public TimePickerDialogFragment (Handler handler) {
//        this.handler = handler;
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Bundle object to pass current set time to fragment
        Bundle bundle = getArguments();

        // Get hour/minute from bundle
        hour = bundle.getInt("set_hour");
        min = bundle.getInt("set_min");

        TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minOfDay) {
                hour = hourOfDay;
                min = minOfDay;

                Bundle bundle = new Bundle();

                bundle.putInt("set_hour", hour);
                bundle.putInt("set_min", min);

                bundle.putString("set_time", "Set Time : " + Integer.toString(hour) + " : " + Integer.toString(min));

                Message message = new Message();

                message.setData(bundle);

                //messsage is sent using the message handler in the main activity (AddReminder)
                timePickHandler.sendMessage(message);
            }
        };

        //Opening the time picker dialog window
        return new TimePickerDialog(getActivity(), listener, hour, min, false);

    }


}
