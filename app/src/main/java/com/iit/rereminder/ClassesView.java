package com.iit.rereminder;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.iit.server.ServerAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ClassesView extends AppCompatActivity {
    
    private ProgressDialog pDialog;
    private ListView lv;

    GlobalData globalData = new GlobalData();
    ArrayList<HashMap<String, String>> classesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_classes_view);

        classesList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.classes_list);

        new GetClasses().execute(globalData.username);
    }

    private class GetClasses extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ClassesView.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            String jsonStr = server.getCoursesRequest(globalData.username);


            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONArray courses = jsonObj.getJSONArray("courses");

                    for (int i = 0; i < courses.length(); i++) {
                        String c = courses.getString(i);

                        HashMap<String, String> course = new HashMap<>();

                        course.put("course", c);

                        classesList.add(course);
                    }

                } catch (final JSONException e) {
                    /*Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                */}
           // } else {
                /*Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            */}
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    ClassesView.this, classesList,
                    R.layout.course_item, new String[]{"course"}, new int[]{R.id.classname});

            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                    int value = position;

                    Intent intent = new Intent(ClassesView.this, ClassesDetailsView.class);
                    intent.putExtra("class", value);
                    System.out.println(value);
                    startActivity(intent);
                }
            });
        }
    }


    public void addClass(View view) {
        //send the user to the add class screen
        Intent intent = new Intent(this, AddClassScreen.class);
        intent.putExtra("modify", 0);
        startActivity(intent);
    }

    public void classToHome(View view) {
        //send the user to the add class screen
        Intent intent = new Intent(this, HomeScreen.class);
        startActivity(intent);
    }
}

