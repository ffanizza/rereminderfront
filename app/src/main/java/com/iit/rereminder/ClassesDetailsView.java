package com.iit.rereminder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.iit.server.ServerAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ClassesDetailsView extends AppCompatActivity {

    private ProgressDialog pDialog;

    GlobalData globalData = new GlobalData();

    int courseRank;
    String coursename;
    String jsonStr;
    String startTime;
    String endTime;
    JSONArray jsonDay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes_details_view);

        courseRank = getIntent().getExtras().getInt("class");

        new GetClassesDetails().execute(globalData.username);
    }

    // Method called when the user presses the modify class button.
    public void ModifyClass(View view) {
        //send the user to the add class screen
        Intent intent = new Intent(this, AddClassScreen.class);
        intent.putExtra("modify", 1); // Flag to notify the next activity to fill fields with the current data of class,
        intent.putExtra("details", jsonStr); // Variable used to store the current Json String containing the details of the class.
        startActivity(intent);
    }


    // Method called when a user presses the delete class button
    public void DropClass(View view){

        new DropClasses().execute(globalData.username);

        Intent intent = new Intent(this, ClassesView.class);
        startActivity(intent);

    }


// -------------------------- ASYNC METHOD TO GET CLASS DETAILS ---------------------------------- //

    private class GetClassesDetails extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ClassesDetailsView.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            jsonStr = server.getCourseDetailsRequest(params[0], courseRank);

            // Parse the Json string
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    coursename = jsonObj.getString("name");
                    startTime = jsonObj.getString("startTime");
                    endTime = jsonObj.getString("endTime");

                    jsonDay = jsonObj.getJSONArray("days");

                    /*for (int i = 0; i < jsonDay.length(); i++) {
                        c = c + "  " + jsonDay.getString(i);
                        days.setText(c);
                    }*/

                } catch (final JSONException e) {
                    /*Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                */
                }
            } else {
                /*Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            */
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            // Set a name for fields
            TextView classnameTv = (TextView) findViewById(R.id.tvClassDetailsName);
            TextView daysTv = (TextView) findViewById(R.id.tvClassDetailsDays);
            TextView startTv = (TextView) findViewById(R.id.tvClassDetailsST);
            TextView endTv = (TextView) findViewById(R.id.tvClassDetailsET);
            String c = new String();

            classnameTv.setText(coursename);
            startTv.setText(startTime);
            endTv.setText(endTime);

            try {
                for (int i = 0; i < jsonDay.length(); i++) {
                    c = c + "  " + jsonDay.getString(i);
                    daysTv.setText(c);
                }
            } catch (final JSONException e) {
            }
        }
    }

// -------------------------- ASYNC METHOD TO DROP CLASS DETAILS ---------------------------------- //

    private class DropClasses extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();
        @Override
        protected Void doInBackground(String... params) {

            server.dropCourseRequest(params[0], courseRank);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        }
    }

}
