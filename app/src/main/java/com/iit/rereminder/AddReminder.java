package com.iit.rereminder;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.iit.server.ServerAction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class AddReminder extends AppCompatActivity {

    private ProgressDialog pDialog;
    GlobalData globalData = new GlobalData();
    List<String> courseList = new ArrayList<String>();
    boolean modify;
    String reminderName;
    String reminderCourse;
    String reminderDueDate;

    String reminderDueTime;

    int reminderID;
    int courseSelectedID;

    private static class reminderData {

        String userNameServer;
        String dueDateServer;
        String dueTimeServer;
        boolean modifyServer;
        String reminderNameServer;
        String reminderCourseServer;
        int reminderIDServer;

        reminderData(String userNameServer, String dueDateServer, String dueTimeServer,
                     boolean modifyServer, String reminderNameServer, String reminderCourseServer,
                     int reminderIDServer) {
            this.userNameServer = userNameServer;
            this.dueDateServer = dueDateServer;
            this.dueTimeServer = dueTimeServer;
            this.modifyServer = modifyServer;
            this.reminderNameServer = reminderNameServer;
            this.reminderCourseServer = reminderCourseServer;
            this.reminderIDServer = reminderIDServer;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_reminder_screen);

        modify = getIntent().getExtras().getBoolean("modify");

        //values for adapter
        new GetSpinnerCourses().execute(GlobalData.username);

        //assigning variable to spinner element
        //Spinner chooseClass = (Spinner) findViewById(R.id.choose_course_spinner);
        //listener (equiv to onClick)
        //chooseClass.setOnItemSelectedListener(this);

        if (modify) {
            //fields
            reminderID = getIntent().getExtras().getInt("reminder");
            EditText reminderName = (EditText) findViewById(R.id.reminder_name);
            Spinner reminderCourse = (Spinner) findViewById(R.id.choose_course_spinner);
            TextView reminderDueDate = (TextView) findViewById(R.id.reminder_due_date_selected);
            TextView reminderDueTime = (TextView) findViewById(R.id.reminder_due_time_selected);
            //assign values to fields
            reminderName.setText(getIntent().getExtras().getString("reminderName"));

            //reminderCourse.setSelection();
            reminderDueDate.setText(getIntent().getExtras().getString("dueDate"));
            reminderDueTime.setText(getIntent().getExtras().getString("dueTime"));
//
        }

    }

    //Asynchronous class to get json
    private class GetSpinnerCourses extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Show progress dialog text
            pDialog = new ProgressDialog(AddReminder.this);
            pDialog.setMessage("Accessing server, please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            //Spinner element (requires Async -> do in background)
            String courseListString = server.getCoursesRequest(GlobalData.username);

            if (courseListString != null) {
                try {
                    JSONObject jsonObjCourses = new JSONObject(courseListString);
                    JSONArray courses = jsonObjCourses.getJSONArray("courses");
                    for (int courseIndex = 0; courseIndex < courses.length(); courseIndex++) {
                        String currCourseName = courses.getString(courseIndex);
                        courseList.add(currCourseName);
                    }
                } catch (final JSONException e) {
                    //if JSON error
                }
            }
            else {
//                //if empty response
//                //no classes to choose from, need to create class first
                String toastString = "(EMPTY String) No classes found, create classes first";
                Toast toast = Toast.makeText(getApplicationContext(),toastString,Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //Dismiss the progress dialog on success
            if (pDialog.isShowing())
                pDialog.dismiss();

            spinnerElement();
        }
    }

    public void spinnerElement() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                AddReminder.this, android.R.layout.simple_spinner_item, courseList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner sItems = (Spinner) findViewById(R.id.choose_course_spinner);
        sItems.setAdapter(adapter);

        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                courseSelectedID = position;
                reminderCourse = parent.getItemAtPosition(position).toString();

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    public boolean checkReminderData() {

        boolean allFormsFilled = true;
        // check if all information is filled with any data
        if (((EditText)findViewById(R.id.reminder_name)).getText().toString().isEmpty()) {
            allFormsFilled = false;
        }
        if (((Spinner)findViewById(R.id.choose_course_spinner)).getSelectedItem().toString().isEmpty()) {
            allFormsFilled = false;
        }
        if (((TextView)findViewById(R.id.reminder_due_date_selected)).getText().toString().isEmpty()) {
            allFormsFilled = false;
        }
        if (((TextView)findViewById(R.id.reminder_due_time_selected)).getText().toString().isEmpty()) {
            allFormsFilled = false;
        }
        return allFormsFilled;
    }

    public void saveReminder(View view) {

        //check if all fields are filled
        if (checkReminderData()) {


            // Update values of reminder variables
            reminderName = ((EditText)findViewById(R.id.reminder_name)).getText().toString();
            reminderCourse = ((Spinner)findViewById(R.id.choose_course_spinner)).getSelectedItem().toString();
            reminderDueDate = ((TextView)findViewById(R.id.reminder_due_date_selected)).getText().toString();
            String dueDateServer = globalData.writtenDateToStd(reminderDueDate);
            reminderDueTime = ((TextView)findViewById(R.id.reminder_due_time_selected)).getText().toString();
            String dueTimeServer = globalData.usTimeToTwentyfour(reminderDueTime);

            //create reminderData class
            reminderData serverInfo = new reminderData(
                    globalData.username,
                    dueDateServer,
                    dueTimeServer,
                    modify,
                    reminderName,
                    reminderCourse,
                    reminderID);

            new CreateModifyReminder().execute(serverInfo);

//            String toastString = "date ="+dueDateServer+"   time ="+dueTimeServer;
//            Toast toast = Toast.makeText(getApplicationContext(),toastString,Toast.LENGTH_LONG);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();

            Intent intent = new Intent(this, ViewReminders.class);
            startActivity(intent);

        } else {
            //show message, "Insufficient data"
//            Toast toast = Toast.makeText(
//                    getApplicationContext(),
//                    "Insufficient data, please make sure all fields are filled",
//                    Toast.LENGTH_LONG);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
        }
    }

    public void cancelReminder(View view) {
        //send the user to the edit reminder screen
//        NotificationCreator noti = new NotificationCreator();
//        noti.notify(this.getBaseContext(),"Test Notification...", 1);
        Intent intent = new Intent(this, ViewReminders.class);
        startActivity(intent);
    }


    //Asyncronous class to create reminder
    private class CreateModifyReminder extends AsyncTask<reminderData, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected Void doInBackground(reminderData... params) {
            try {
                if (!params[0].modifyServer) {
                    server.addReminderRequest(
                            params[0].userNameServer,
                            params[0].reminderNameServer,
                            params[0].reminderCourseServer,
                            params[0].dueDateServer,
                            params[0].dueTimeServer);
                    return null;
                } else {
                    server.modifyReminderRequest(
                            params[0].userNameServer,
                            params[0].reminderIDServer,
                            params[0].reminderNameServer,
                            params[0].reminderCourseServer,
                            params[0].dueDateServer,
                            params[0].dueTimeServer);
                    return null;
                }
            } catch (Exception e) {
                //
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //Dismiss the progress dialog on success
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }


            // ###### Fragment used for the time picker ######
    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use 11:50PM as the default time for the picker
            int hours = 23;
            int minutes = 50;

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hours, minutes,
                    DateFormat.is24HourFormat(getActivity()));
        }

        // Do something with the time chosen by the user
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            TextView timeSelected = (TextView) getActivity().findViewById(R.id.reminder_due_time_selected);
            GlobalData globalData = new GlobalData();
            String minuteString = globalData.minuteToString(minute);
            String aMpM = globalData.determineAmPm(hourOfDay);  //Get the AM or PM for current time
            int hourUS = globalData.convertFormatHourTest(hourOfDay);    //Make the 24 hour time format to 12 hour time format
            timeSelected.setText(String.valueOf(hourUS) + ":" + minuteString +" "+ aMpM); //Display the user changed time on TextView
//            dueTimeServer = globalData.convertMillisecond(hourOfDay, minute);   // Convert hour to millisecond to send to backend
        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new AddReminder.TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    // End timePicker fragment

    // ###### Fragment used for the date picker ######
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // TODO: Use the current date as the default values for the picker
            int year = 2016; // ==2016
            int month = 10; //10 == November // NOTE: 12 == 0 == January
            int day = 28; // == 28

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        // Do something with the time chosen by the user
        public void onDateSet(DatePicker view, int year, int month, int day) {
            TextView dateSelected = (TextView) getActivity().findViewById(R.id.reminder_due_date_selected);
            GlobalData globalData = new GlobalData();
            dateSelected.setText(globalData.writtenMonth(month)+" "+Integer.toString(day)+", "+Integer.toString(year));
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new AddReminder.DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }
    // End datePicker Fragment

}
