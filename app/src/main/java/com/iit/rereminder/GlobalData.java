package com.iit.rereminder;

import android.os.AsyncTask;
import android.util.StringBuilderPrinter;
import android.widget.TextView;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

import com.iit.server.ServerAction;

/**
 * Created by Thibault on 14/11/2016.
 */

public class GlobalData {

    // Global variables used in most of requests.
    // Updated once the authentication is done.
    public static String username;

    // Method to test id a field is empty
    public int empty_field(String content, String message, TextView id) {
        if (!content.isEmpty()) {
            return 0;
        } else {
            id.setText(message);
            return 1;
        }
    }

    // Convert Time to Milliseconds
    public long convertMillisecond(int hours, int minutes){
        return hours*3600*1000 + minutes*60*1000;
    }

    // Make the 24 hour time format to 12 hour time format
    public int convertFormatHour(int hours){
        int currentHour;
        if(hours>11)
        {
            currentHour = hours - 12;
        }
        else
        {
            currentHour = hours;
        }
        return currentHour;
    }

    public int convertFormatHourTest(int hour){
        int currHour = hour;
        if (hour == 0) {
            currHour += 12;
        } else if (hour > 12) {
            currHour -= 12;
        }
        return currHour;
    }

    public String minuteToString(int minute) {
        if (minute < 10) {
            return "0" + Integer.toString(minute);
        } else {
            return  Integer.toString(minute);
        }
    }

    // Get the AM or PM for current time
    public String determineAmPm(int hours){
        String aMpM = "AM";
        if(hours >11)
        {
            aMpM = "PM";
        }
        return aMpM;
    }

    public String writtenMonth(int month) {
        switch (month){
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
            default:
                return "what the...";
        }
    }

    public String writtenDateToStd (String writtenDate) {
        //format: Month D(D), YYYY
        String[] dateArray = writtenDate.split(" ");
        String writtenMonth = dateArray[0]; //convert to numeric representation
        String writtenDay = dateArray[1]; //remove comma, add '0' if necessary
        String year = dateArray[2]; //ok as is
        String month = "00";
        if (writtenMonth.equalsIgnoreCase("January")) { month = "01"; }
        if (writtenMonth.equalsIgnoreCase("February")) { month = "02"; }
        if (writtenMonth.equalsIgnoreCase("March")) { month = "03"; }
        if (writtenMonth.equalsIgnoreCase("April")) { month = "04"; }
        if (writtenMonth.equalsIgnoreCase("May")) { month = "05"; }
        if (writtenMonth.equalsIgnoreCase("June")) { month = "06"; }
        if (writtenMonth.equalsIgnoreCase("July")) { month = "07"; }
        if (writtenMonth.equalsIgnoreCase("August")) { month = "08"; }
        if (writtenMonth.equalsIgnoreCase("September")) { month = "09"; }
        if (writtenMonth.equalsIgnoreCase("October")) { month = "10"; }
        if (writtenMonth.equalsIgnoreCase("November")) { month = "11"; }
        if (writtenMonth.equalsIgnoreCase("December")) { month = "12"; }
        String day = "00";
        String[] parsedDay = writtenDay.split(",");
        if (parsedDay[0].length() == 1) {
            day = "0"+parsedDay[0];
        } else {
            day = parsedDay[0];
        }
        return year+"-"+month+"-"+day;
    }

    public String usTimeToTwentyfour (String usTime) {
        //format: 1:24 PM
        // -> H(H):MM AM (space before AM/PM)
        String[] hourParse = usTime.split(":");
        String usHour = hourParse[0];
        String[] minuteAMPMParse = hourParse[1].split(" ");
        String minute = minuteAMPMParse[0];
        String amPM = minuteAMPMParse[1];
        String hour = "00";
        if (amPM.equals("PM")) {
            if (usHour.equals("12")) {
                hour = "12";
            } else {
                hour = Integer.toString(Integer.parseInt(usHour)+12);
            }
        } else {
            if (Integer.parseInt(usHour) < 10) {
                hour = "0"+usHour;
            } else if (usHour.equals("12")){
                hour = "00";
            } else {
                hour = usHour;
            }
        }
        return hour+":"+minute;
    }


    public String convertMilliToYYYYMMdd(long millisecDate) {
        Date date = new Date(millisecDate);

        SimpleDateFormat formattedDate = new SimpleDateFormat("YYYY-MM-dd");

        return formattedDate.format(date); //returns date in YYYY-MM-DD format. ex: 1970-01-01
    }

    public String convertMilliToHHmm(long millisecDate) {
        Date date = new Date(millisecDate);

        SimpleDateFormat formattedTime = new SimpleDateFormat ("HH:mm");

        return formattedTime.format(date); //returns time in 24 hr format. ex: 23:59, 01:05
    }

    public static final String md5Encrypt(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return "";
        }
    }
}
