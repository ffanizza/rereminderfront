package com.iit.rereminder;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.View;
import android.app.DialogFragment;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.iit.server.ServerAction;

import java.util.ArrayList;


public class AddClassScreen extends AppCompatActivity {


    // Global variables used in this acticity
    GlobalData globalData = new GlobalData();
    ServerAction server = new ServerAction();
    static String  startTime;
    static String endTime;

    // Class used to pass different type of arguments in the async task.
    private static class taskParams {
        int modified;
        String usernameSent;
        int courseIDSent;
        String classnameSent;
        String startTimeSent;
        String endTimeSent;
        ArrayList<String> daysSent;

        taskParams(int modified, String usernameSent, int coursIDSent, String classnameSent,
                   String startTimeSent, String endTimeSent, ArrayList<String> daysSent) {
            this.modified = modified;
            this.usernameSent = usernameSent;
            this.courseIDSent = coursIDSent;
            this.classnameSent = classnameSent;
            this.startTimeSent = startTimeSent;
            this.endTimeSent = endTimeSent;
            this.daysSent = daysSent;
        }
    }





// -------------------------------------- CREATION OF THE ACTIVITY ------------------------------------- //
    // Actions performed when the activity is called
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class_screen);

        int modify = getIntent().getExtras().getInt("modify"); // Get the flag to kow if the fields have to be pre-filled.

        // Distinction between the creation and the modification regarding the value of the flag
        if(modify == 1){

            // Variable used to fill the fields if we modify a course
            String details = getIntent().getExtras().getString("details");
            EditText classname = (EditText)findViewById(R.id.classidlabel);
            TextView starttime = (TextView)findViewById(R.id.starttimefield);
            TextView endtime = (TextView)findViewById(R.id.endtimefield);
            CheckBox Mon = (CheckBox)findViewById(R.id.addmonday);
            CheckBox Tue = (CheckBox)findViewById(R.id.addtuesday);
            CheckBox Wed = (CheckBox)findViewById(R.id.addwednesday);
            CheckBox Thu = (CheckBox)findViewById(R.id.addthursday);
            CheckBox Fri = (CheckBox)findViewById(R.id.addfriday);
            CheckBox Sat = (CheckBox)findViewById(R.id.addsaturday);

            // Parse the Json string from the details Extra
            try {
                JSONObject jsonObj = new JSONObject(details);
                classname.setText(jsonObj.getString("name"));

                starttime.setText(jsonObj.getString("startTime"));
                endtime.setText(jsonObj.getString("endTime"));

                JSONArray jsonDay = jsonObj.getJSONArray("days");

                for (int i = 0; i < jsonDay.length(); i++) {
                    if(jsonDay.getString(i).equals("Monday")){
                        Mon.setChecked(true);
                    }
                    else if(jsonDay.getString(i).equals("Tuesday")) {
                        Tue.setChecked(true);
                    }
                    else if(jsonDay.getString(i).equals("Wednesday")) {
                        Wed.setChecked(true);
                    }
                    else if(jsonDay.getString(i).equals("Thursday")){
                        Thu.setChecked(true);
                    }
                    else if(jsonDay.getString(i).equals("Friday")){
                        Fri.setChecked(true);
                    }
                    else if(jsonDay.getString(i).equals("Saturday")) {
                        Sat.setChecked(true);
                    }
                }

            }catch (final JSONException e) {}
        }
        Intent intent = getIntent();
    }

    // Action performed when click on save button
    public void saveclassbutton(View view){
        // Add here the action of backend to make when the button is pressed
        // Eventually display a pop up screen to confirm the class has been added
        int box_checked = 0;
        TextView alert = (TextView)findViewById(R.id.tvAlertclasscreation);
        int flag_name = 0;
        int flag_start = 0;
        int flag_end = 0;
        int flag_days = 0;

        //JSONArray days = new JSONArray();
        EditText classname = (EditText)findViewById(R.id.classidlabel);
        TextView starttime = (TextView)findViewById(R.id.starttimefield);
        TextView endtime = (TextView)findViewById(R.id.endtimefield);
        CheckBox Mon = (CheckBox)findViewById(R.id.addmonday);
        CheckBox Tue = (CheckBox)findViewById(R.id.addtuesday);
        CheckBox Wed = (CheckBox)findViewById(R.id.addwednesday);
        CheckBox Thu = (CheckBox)findViewById(R.id.addthursday);
        CheckBox Fri = (CheckBox)findViewById(R.id.addfriday);
        CheckBox Sat = (CheckBox)findViewById(R.id.addsaturday);
        ArrayList<String> days = new ArrayList<String>();

        flag_end = globalData.empty_field(endtime.getText().toString(), "Enter an end time", alert);
        flag_start = globalData.empty_field(starttime.getText().toString(), "Enter a start time", alert);

        if(Mon.isChecked()){
            days.add("Monday");
            box_checked ++;
        }

        if(Tue.isChecked()){
            days.add("Tuesday");
            box_checked ++;
        }

        if(Wed.isChecked()){
            days.add("Wednesday");
            box_checked ++;
        }

        if(Thu.isChecked()){
            days.add("Thursday");
            box_checked ++;
        }

        if(Fri.isChecked()) {
            days.add("Friday");
            box_checked++;
        }

        if(Sat.isChecked()){
            days.add("Saturday");
            box_checked ++;
        }

        if(box_checked != 0){
            flag_days = 0;
        }else{
            alert.setText("Select at least on day for your class");
            flag_days = 1;
        }

        flag_name = globalData.empty_field(classname.getText().toString(), "Enter a class name", alert);

        // Send the request to the server according to the type of action performed: modification or addition
        // TODO Check if
        if(flag_days == 0 && flag_end == 0 && flag_start == 0 && flag_name == 0){
            int modify = getIntent().getExtras().getInt("modify");

            sendRequest request = new sendRequest();
            taskParams addtask = new taskParams(modify, globalData.username, 0,  classname.getText().toString(), startTime, endTime, days);
            request.execute(addtask);

            Intent intent = new Intent(this, ClassesView.class);
            startActivity(intent);
        }

    }

    // Action performed on click on cancel Button
    public void CancelAddClassButton(View view){
        AddClassScreen.this.finish();
    }


// ------------------------------------------- FRAGMENT CODE ---------------------------------------------------------- //
    // ###### Fragment used for the start time parameter ######
    public static class StartTimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            long time = System.currentTimeMillis();
            int minutes = (int) ((time/ (1000*60)) % 60);
            int hours   = (int) ((time / (1000*60*60)) % 24) - 6;

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hours, minutes,
                    DateFormat.is24HourFormat(getActivity()));
        }

        // Do something with the time chosen by the user
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            TextView starttime = (TextView) getActivity().findViewById(R.id.starttimefield);
            GlobalData globalData = new GlobalData();

            String aMpM = globalData.determineAmPm(hourOfDay);  //Get the AM or PM for current time
            int currentHour = globalData.convertFormatHour(hourOfDay);  //Make the 24 hour time format to 12 hour time format
            startTime = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
            starttime.setText(String.valueOf(currentHour) + ":" + String.valueOf(minute) + aMpM); //Display the user changed time on TextView
        }
    }

    public void showStartTimePickerDialog(View v) {
        DialogFragment newFragment = new StartTimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    // End Fragment start time

    // ###### Fragment used for the end time parameter ######
    public static class EndTimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            long time = System.currentTimeMillis();
            int minutes = (int) ((time/(1000*60)) % 60);
            int hours   = (int) ((time / (1000*60*60)) % 24) - 6;

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hours, minutes,
                    DateFormat.is24HourFormat(getActivity()));

        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            TextView endtime = (TextView) getActivity().findViewById(R.id.endtimefield);
            GlobalData globalData = new GlobalData();

            String aMpM = globalData.determineAmPm(hourOfDay);  //Get the AM or PM for current time
            int currentHour = globalData.convertFormatHour(hourOfDay);  //Make the 24 hour time format to 12 hour time format
            endtime.setText(String.valueOf(currentHour) + ":" + String.valueOf(minute) + aMpM);
            //Display the user changed time on TextView
            endTime = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
            globalData.convertMillisecond(hourOfDay,minute);  //Convert the time in milliseconds to send to the backend
        }
    }

    public void showEndTimePickerDialog(View v) {
        DialogFragment newFragment = new EndTimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    // End Fragment end time





// ------------------------------------------ ASYNC TASK FOR HTTP REQUEST -------------------------------------------//
    // Async task to perform the http request
    private class sendRequest extends AsyncTask<taskParams, Void, Void> {

        ServerAction server = new ServerAction();
        @Override
        protected Void doInBackground(taskParams... params) {

            if(params[0].modified == 0) {
                // The param course ID is not useful in this request
                server.addCourseRequest(params[0].usernameSent, params[0].classnameSent, params[0].startTimeSent, params[0].endTimeSent, params[0].daysSent);
           }
            else if(params[0].modified == 1){
                // The param course ID is used in this request
                server.modifyCourseRequest(params[0].usernameSent, params[0].courseIDSent, params[0].classnameSent, params[0].startTimeSent, params[0].endTimeSent, params[0].daysSent);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        }
    }
}
