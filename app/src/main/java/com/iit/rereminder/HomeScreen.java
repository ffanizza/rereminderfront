package com.iit.rereminder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
    }

    // Called when user clicks the View Reminder button
    public void viewReminders(View view) {
        //send the user to the view classes screen
        Intent intent = new Intent(this, ViewReminders.class);
        startActivity(intent);
    }

    // Called when the user press the View classes button
    public void viewClasses(View view){
        // Send the user to the view classes screen
        Intent intent = new Intent(this, ClassesView.class);
        startActivity(intent);
    }
}
