package com.iit.rereminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.iit.server.ServerAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Frank on 2016-11-25.
 */

public class NotiAlarmManager extends Activity {

    GlobalData globalData = new GlobalData();
    ProgressDialog pDialog;
    ArrayList<HashMap<String, Long>> notificationList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main);

        //get information from server
        new GetNotificationJSON().execute(globalData.username);


    }

    private class GetNotificationJSON extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Show progress dialog text
            pDialog = new ProgressDialog(getBaseContext());
            pDialog.setMessage("Accessing server, please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            //Get Notifications
            String notificationJSONString = server.getNotificationsRequest(params[0]);
            if (notificationJSONString != null &&
                    !notificationJSONString.isEmpty() &&
                    notificationJSONString != "") {
                try {
                    JSONObject notificationJSONObj = new JSONObject(notificationJSONString);
                    JSONArray notificationJSONArray = notificationJSONObj.getJSONArray("notifications");
                    for (int notiID = 0; notiID < notificationJSONArray.length(); notiID++) {
                        //Create notiAlarm for each received notification (use "name" and "time")
//                        NotiAlarm notiAlarm = new NotiAlarm();

                    }
                } catch (final JSONException e) {
                    //if JSON error
                }
            } else {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
            //Dismiss the progress dialog on success
            if (pDialog.isShowing())
                pDialog.dismiss();
        }

    }




    Intent alarmIntent = new Intent(this, NotiAlarm.class);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//    alarmManager.set(AlarmManager.RTC_WAKEUP, notificationTime, pendingIntent);
//
//    Context.getSystemService(Context.ALARM_SERVICE);  //TODO <- use this to act as trigger for notification creation

}
