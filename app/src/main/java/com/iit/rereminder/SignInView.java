package com.iit.rereminder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.iit.server.ServerAction;
import com.iit.rereminder.GlobalData;

import java.util.ArrayList;
import java.util.HashMap;

public class SignInView extends AppCompatActivity {

    GlobalData globalData = new GlobalData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_view);
    }

    public void loginbutton(View view){
        // Local variables used to handle exception (empty fields).
        TextView alert = (TextView)findViewById(R.id.tvAlertUserRegistration);
        int flag_username = 0;
        int flag_email = 0;
        int flag_password = 0;

        // Local variables for the fields
        EditText username = (EditText)findViewById(R.id.username);
        EditText email = (EditText) findViewById(R.id.email);
        EditText password = (EditText) findViewById(R.id.password);
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z.-_]+\\.+[a-z]+";

        // Flag used to check if fields are empty.
        flag_password = globalData.empty_field(password.getText().toString(), "Enter a password", alert);
        flag_username = globalData.empty_field(password.getText().toString(), "Enter a username", alert);

        // Verification of email pattern
        if(email.getText().toString().trim().matches(emailPattern) && !email.getText().toString().isEmpty()){
            flag_email = 0;
        }else{
            alert.setText("Enter a valid email");
            flag_email = 1;
        }

        // Send the request to the backend
        if(flag_username == 0 && flag_email == 0 && flag_password == 0){
            sendRequest request = new sendRequest();
            GlobalData globalData = new GlobalData();

            String passwordHashed = globalData.md5Encrypt(password.getText().toString());

            request.execute(username.getText().toString(), email.getText().toString(), passwordHashed);

            Intent intent = new Intent(this, ConnexionScreen.class);
            startActivity(intent);
        }
    }

    private class sendRequest extends AsyncTask< String, Void, Void> {

        ServerAction server = new ServerAction();
        @Override
        protected Void doInBackground(String... params) {
            server.createUserRequest(params[0], params[1], params[2]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        }
    }
}
