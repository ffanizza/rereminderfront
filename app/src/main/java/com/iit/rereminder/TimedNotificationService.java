package com.iit.rereminder;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Frank on 2016-11-25.
 */

public class TimedNotificationService extends IntentService {

    public TimedNotificationService() {
        super("TimedNotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String notificationService = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(notificationService);

        CharSequence tickerText = "Notification message";
        long currTime = System.currentTimeMillis();

        NotificationCreator notificationCreator = new NotificationCreator();

        Context context = getApplicationContext();
        CharSequence contentTitle = "Notification title";
        CharSequence contentText = "Notification body";
        //Intent notificationIntent = new Intent(this, Destination.class);
        Bundle deadlineBundle = new Bundle();

        //PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        //notificationCreator.setDeadlineInfo(context, contentTitle, contentText, contentIntent);

        notificationCreator.notify(this.getBaseContext(), "Notification test", 1);


        Intent alarmIntent = new Intent(this, NotiAlarm.class);
        long alarmTime = 1000000;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);

        stopService(intent);

    }






}
