package com.iit.rereminder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.iit.server.ServerAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Frank on 2016-10-23.
 */

public class ReminderExistingView extends AppCompatActivity {


    private ProgressDialog pDialog;
    GlobalData globalData = new GlobalData();

    int reminderID;
    String reminderName;
    String courseName;
    String reminderDetailsJSONStr;
    boolean reminderIsComplete;
    String dueDate;
    String dueTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_existing_view);

        reminderID = getIntent().getExtras().getInt("reminder");

        new GetRemainingDetails().execute(globalData.username);
    }

    //ASYNC FUNCTION TO GET JSON
    private class GetRemainingDetails extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ReminderExistingView.this);
            pDialog.setMessage("Accessing server, please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            reminderDetailsJSONStr = server.getReminderDetails(params[0], reminderID);

            System.out.println(reminderDetailsJSONStr);
            // Parse the Json string
            if (reminderDetailsJSONStr != null) {
                try {
                    JSONObject reminderDetailsJSONObj = new JSONObject(reminderDetailsJSONStr);

                    reminderIsComplete = reminderDetailsJSONObj.getBoolean("complete");
                    reminderName = reminderDetailsJSONObj.getString("content");
                    dueDate = reminderDetailsJSONObj.getString("date");
                    dueTime = reminderDetailsJSONObj.getString("time");
                    courseName = reminderDetailsJSONObj.getString("course");

                } catch (final JSONException e) {
                    //reminder parse exception
                }
            } else {
                // reminder empty exception
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            // Set a name for fields
            TextView tvReminderName = (TextView)findViewById(R.id.tv_reminder_existing_name);
            TextView tvReminderCourse = (TextView)findViewById(R.id.tv_reminder_course);
            TextView tvReminderDueDate = (TextView) findViewById(R.id.due_date);
            TextView tvReminderDueTime = (TextView) findViewById(R.id.reminder_due_time);
            CheckBox checkBoxComplete = (CheckBox)findViewById(R.id.checkbox_is_complete);
            //fill fields
            tvReminderName.setText(reminderName);
            tvReminderCourse.setText(courseName);
            tvReminderDueDate.setText(dueDate);
            tvReminderDueTime.setText(dueTime);
            checkBoxComplete.setChecked(reminderIsComplete);
        }

    }

//    public void onCompleteCheckBox(View view) {
//        CheckBox checkBoxComplete = (CheckBox)findViewById(R.id.checkbox_is_complete);
//        boolean complete = checkBoxComplete.isChecked();
//    }


    //send user to ViewReminders after updating reminder (complete or not complete):
    public void saveReminder (View view) {

        CheckBox checkBoxCompleted = (CheckBox)findViewById(R.id.checkbox_is_complete);
        reminderIsComplete = checkBoxCompleted.isChecked();

        new ModifyReminderAsync().execute(globalData.username);


        Intent intent = new Intent(this, ViewReminders.class);
        startActivity(intent);

    }




    // --------------------- ASYNC FOR MODIFYING REMINDERS ------------------------------------ ::
    private class ModifyReminderAsync extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected Void doInBackground(String... params) {

            if (reminderIsComplete) {
                server.completeReminderRequest(params[0], reminderID);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
        }

    }

    // send user to AddReminders with:
    // variable denoting an existing reminder to be modified, and current values
    public void modifyReminder (View view) {
        Intent intent = new Intent(this, AddReminder.class);
        intent.putExtra("modify", true);
        intent.putExtra("reminderName", reminderName);
        intent.putExtra("courseName", courseName);
        intent.putExtra("dueDate", dueDate);
        intent.putExtra("dueTime", dueTime);
        startActivity(intent);
    }

    //send user back to view reminder screen
    public void cancel (View view) {
        Intent intent = new Intent(this, ViewReminders.class);
        startActivity(intent);
    }
}
