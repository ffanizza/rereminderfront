package com.iit.rereminder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.iit.server.ServerAction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class ViewReminders extends AppCompatActivity {

    private ProgressDialog pDialog;
    private ListView lv;
    ArrayList<HashMap<String, String>> reminderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reminder_screen);

        reminderList = new ArrayList<>();

        lv = (ListView)findViewById(R.id.reminder_list_view);

        new GetReminders().execute();
    }

    //Asyncronous class to get json
    private class GetReminders extends AsyncTask<String, Void, Void> {

        ServerAction server = new ServerAction();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Show progress dialog text
            pDialog = new ProgressDialog(ViewReminders.this);
            pDialog.setMessage("Accessing server, please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... param) {

            String jsonStringReminders = server.getActiveRemindersRequest(GlobalData.username);
            //pull reminders from server
            System.out.println(jsonStringReminders);
            if (jsonStringReminders != null) {
                try {
                    JSONObject jsonObjectReminders = new JSONObject(jsonStringReminders);
                    JSONArray jsonArrayReminders = jsonObjectReminders.getJSONArray("reminders");
                    //iterating reminders

                    for (int i = 0; i < jsonArrayReminders.length(); i++) {

                        String currReminderString = jsonArrayReminders.getString(i);

                        System.out.println(currReminderString);

                        HashMap<String, String>  reminder = new HashMap<>();
                        reminder.put("Reminder Name",currReminderString);

                        reminderList.add(reminder);
                    }
                } catch (JSONException e) {
//                    //TODO catch block
//                    Toast toast = Toast.makeText(
//                            getApplicationContext(),
//                            "JSON error",
//                            Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
                }
            } else {
                //
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //Dismiss the progress dialog on success
            if (pDialog.isShowing())
                pDialog.dismiss();

            //updating parsed json into listview
            ListAdapter adapter = new SimpleAdapter(
                    ViewReminders.this,
                    reminderList,
                    R.layout.reminder_list_item,
                    new String[]{"Reminder Name"},
                    new int[]{R.id.reminder_name}
            );

            lv.setAdapter(adapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent intent = new Intent(ViewReminders.this, ReminderExistingView.class);

                    intent.putExtra("reminder",position);
                    String reminderNameValue = ((TextView) view.findViewById(R.id.reminder_name)).getText().toString();
                    intent.putExtra("reminderName", reminderNameValue);

                    startActivity(intent);
                }
            });
        }
    }

    // Called when user clicks the New Reminder button
    public void addReminder(View view) {
        //send the user to the AddReminder screen
        Intent intent = new Intent(this, AddReminder.class);
        intent.putExtra("modify",false);
        startActivity(intent);
    }

    // Called when user clicks the Back button
    public void goToHomeScreen(View view) {
        //send the user to the Home screen
        Intent intent = new Intent(this, HomeScreen.class);
        startActivity(intent);
    }

}
