package com.iit.rereminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iit.server.ServerAction;

public class ConnexionScreen extends AppCompatActivity {

    ServerAction server = new ServerAction();
    GlobalData globalData = new GlobalData();

    // variables used by the alarm manager to raise the server request
    private PendingIntent pendingIntent;
    private AlarmManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Alarm request variables
        Intent alarmIntent = new Intent(this, ScheduledTask.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

        setContentView(R.layout.activity_connexion_screen);
    }


// ----------------------------------- ACTION PERFORMED WHEN CLICK ON LOGIN --------------------------------- //
    public void HomeScreen(View view) {
        TextView alert = (TextView)findViewById(R.id.tvAlertConnexion);
        int flag_username = 0;
        int flag_password = 0;

        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);

        flag_password = globalData.empty_field(password.getText().toString(), "Enter a password", alert);

        flag_username = globalData.empty_field(username.getText().toString(), "Enter a username", alert);

        // The flags are useful to test if all fields have been completed. If not, the user will not access the next activity. And an alert will be raised.
        if(flag_username == 0 && flag_password == 0){

            sendRequest request = new sendRequest();
            Boolean tru = true;

            String passwordHashed = globalData.md5Encrypt(password.getText().toString());
            System.out.println(passwordHashed); //TODO Remove

            // TODO make a test to be sure it's a success
            //if(request.execute("loginRequest", username.getText().toString(), passwordHashed) == tru ){
                // Send the user to the Home Screen

            globalData.username = username.getText().toString();

                // If the app is launched for the first time, the request is made immediately.
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            if(!prefs.getBoolean("firstTime", false)){
                    makeRequest();
                }
                else{
                    System.out.println("Not the first time"); // Use to debug
                }


                Intent intent = new Intent(this, HomeScreen.class);
                startActivity(intent);
            /*}else {
                alert.setText("Incorrect login or password"); // Alert message in case password and username are incorrect.
            }*/
        }
    }

    public void SignUpView(View view) {
        // Send the user to the Sign In Screen
        Intent intent = new Intent(this, SignInView.class);
        startActivity(intent);
    }



// ------------------------------------ SET UP THE SCHEDULED NOTIFICATIONS SENDER -------------------------------- //
    // This method sends a signal to the server everyday at midnight, even if the app is running in background
    public void makeRequest(){

        sendRequest request = new sendRequest();
        request.execute("notifRequest", globalData.username);

        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        //int interval = 1000; // Useless for the moment
        manager.setRepeating(AlarmManager.RTC_WAKEUP, 1, AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(this, "Alarm set", Toast.LENGTH_SHORT).show();
    }




// ---------------------------------- BACKGROUND TASKS TO SEND THE REQUESTS TO THE SERVER ------------------------------ //
    private class sendRequest extends AsyncTask< String, Void, Void> {

        ServerAction server = new ServerAction();
        GlobalData globalData = new GlobalData();
        //String jsonStr = server.getCoursesRequest(globalData.username);


        // Send the requests to the server
        @Override
        protected Void doInBackground(String... params) {

            // Login request
            if (params[0].equals("loginRequest")) {
                if (server.loginRequest(params[1], params[2]) == "true"){
                    System.out.println("loginRequest");
                }
            }

            // Notification request of it's the first tIme launching the app
            if (params[0].equals("notifRequest")) {
                server.getNotificationsRequest(globalData.username); //TODO complete
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        }
    }
}
