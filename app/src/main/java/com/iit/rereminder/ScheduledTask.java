package com.iit.rereminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.iit.server.ServerAction;

/**
 * Created by Thibault on 28/10/2016.
 */

public class ScheduledTask extends BroadcastReceiver {

    GlobalData globalData = new GlobalData();
    @Override
    public void onReceive(Context context, Intent intent) {
        // Display a message in the console when the signal is received.
        sendRequest request = new sendRequest();
        ServerAction server = new ServerAction();
        request.execute(globalData.username);
        System.out.println("Signal received");
    }

    // ---------------------------------- BACKGROUND TASKS TO SEND THE REQUESTS TO THE SERVER ------------------------------ //
    private class sendRequest extends AsyncTask< String, Void, Void> {

        ServerAction server = new ServerAction();
        GlobalData globalData = new GlobalData();
        //String jsonStr = server.getCoursesRequest(globalData.username);


        // Send the requests to the server
        @Override
        protected Void doInBackground(String... params) {

            // Notification request of it's the first tIme launching the app
            server.getNotificationsRequest(globalData.username); //TODO complete with test

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        }
    }
}